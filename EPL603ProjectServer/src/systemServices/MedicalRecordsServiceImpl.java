package systemServices;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import customDataTypes.User;

public class MedicalRecordsServiceImpl implements MedicalRecordsService{
	
	Connection conn; 
	private void connect(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/EPL603?user=sqluser&password=sqluserpw");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public User setUser(String user, String pass) {
		
		User userStr = new User();
		connect();
		
		try {
			Statement stm = conn.createStatement();
			ResultSet result = stm.executeQuery("SELECT * FROM users where user_id='"+user+"' and password='"+pass+"'");
			while (result.next()){
				userStr.setUserId(result.getString("user_id"));
				userStr.setUserName(result.getString("name"));
				userStr.setUserSurname(result.getString("surname"));
				userStr.setUserClinic(result.getString("clinic_id"));
				userStr.setUserType(result.getInt("type"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Got an exception at users! ");
		    System.err.println(e.getMessage());
		}
			
		return userStr;
	}

	public String setRequest(String request) {
		String responce = null;
		// TODO Auto-generated method stub
		// TODO not sure how to use this, maybe as median for each transaction (see exercise description)
		return responce;
	}

	public void addTransaction(String transaction) {
		// TODO Auto-generated method stub
		// TODO use info from "setRequest" to keep track of transactions, or use directly as median at each transaction
		// TODO may write to file or use new database table
		
	}

	public User getUser(String userId) {
		
		User user = new User();
		try {
			Statement stm = conn.createStatement();
			ResultSet result = stm.executeQuery("SELECT * FROM users where user_id='"+userId+"'");
			while (result.next()){
				user.setUserId(result.getString("user_id"));
				user.setUserName(result.getString("name"));
				user.setUserSurname(result.getString("surname"));
				user.setUserClinic(result.getString("clinic_id"));
				user.setUserType(result.getInt("type"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Got an exception at users! ");
		    System.err.println(e.getMessage());
		}
		return user;
	}

}
