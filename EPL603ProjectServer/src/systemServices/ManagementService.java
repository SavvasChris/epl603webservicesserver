package systemServices;

import customDataTypes.ConditionsAndTreatments;
import customDataTypes.ReportData;

public interface ManagementService {
	public ConditionsAndTreatments getDBPatientsConditionsAndTreatments(String request);
	public ReportData getDBNumberOfPatientsEachDay(String request);
	public ReportData getDBNumberOfPatientsEachMonth(String request);
	public ReportData getDBNumberOfPatientsEachCondition(String request);
	public ReportData getDBNumberOfEachDrugPrescribed(String request);
}
