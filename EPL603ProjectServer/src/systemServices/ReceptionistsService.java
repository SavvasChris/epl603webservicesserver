package systemServices;

import customDataTypes.Asthenis;
import customDataTypes.Kratisi;

public interface ReceptionistsService {
	public Kratisi[] getDBAppointments(String date);
	public Asthenis getDBPatientRecord(int patientId);
	public String setDBPatientAppointment(Kratisi appointment);	
	public String setDBPatient(Asthenis patient);
}
