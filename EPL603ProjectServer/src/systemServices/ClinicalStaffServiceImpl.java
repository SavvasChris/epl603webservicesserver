package systemServices;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement; // Use for the insert

import customDataTypes.Asthenis;
import customDataTypes.Randevu;
import customDataTypes.Treatments;

public class ClinicalStaffServiceImpl implements ClinicalStaffService{
	
	Connection conn; 
	private void connect(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/epl603?user=sqluser&password=sqluserpw");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public Asthenis getDBPatientRecord(int patientId) {
		System.out.println("Clinical staff get patient record " + patientId);
		Asthenis patientInfo = new Asthenis();
		connect();
		try {
			Statement stm = conn.createStatement();
			ResultSet result = stm.executeQuery("SELECT * FROM EPL603.patient where id_patient="+patientId);
			while (result.next()){
				
				patientInfo.setAsthenisId(result.getInt("ID_PATIENT"));
				patientInfo.setAsthenisName(result.getString("NAME"));
				patientInfo.setAsthenisSurname(result.getString("SURNAME"));
				patientInfo.setAsthenisAddress(result.getString("ADREESS"));
				patientInfo.setAsthenisPhone(result.getString("PHONE"));
				patientInfo.setAsthenisRiskStatus(result.getInt("RISK_STATUS"));
				patientInfo.setAsthenisAllergies(result.getString("ALLERGIES"));
				patientInfo.setAsthenisStatus(result.getInt("STATUS"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Got an exception at patient! ");
		    System.err.println(e.getMessage());
		}
		
		return patientInfo;
	}

	public Treatments getDBTreatmentsInfo(String request) {
		System.out.println("Clinical staff get treatments info " + request);
		Treatments treatments = new Treatments();
		ArrayList<String> drug = new ArrayList<String>();
		ArrayList<String> side_effects = new ArrayList<String>();
		connect();		
		try {
			Statement stm = conn.createStatement();
			ResultSet result = stm.executeQuery("SELECT * FROM EPL603.treatment");
			while (result.next()){
				drug.add(result.getString("DRUG"));
				side_effects.add(result.getString("COMMENT"));					
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Got an exception at treatment! ");
		    System.err.println(e.getMessage());
		}
		
		treatments.setDrug(drug.toArray(new String[drug.size()]));
		treatments.setSideEffects(side_effects.toArray(new String[drug.size()]));
		return treatments;
	}

	public Randevu getDBSessionInfo(int patientId, String sessionDate) {
		
		Randevu session = new Randevu();
		connect();
		try {
			Statement stm = conn.createStatement();
			ResultSet result = stm.executeQuery("SELECT a.ID_SESSION, a.DIAGNOSIS, a.ID_PATIENT, a.DATE , a.DRAFT_UPDATED, a.NOTES, a.USER_ID ,a.TREATMENT_ID, a.USERNAME , b.drug, b.comment FROM EPL603.session a , EPL603.treatment b where a.treatment_id = b.treatment_id and a.id_patient="+ patientId +" and a.date ='"+ sessionDate +"'");
			while (result.next()){
				
				session.setRandevuId(result.getInt("a.ID_SESSION"));
				session.setRandevuCondition(result.getString("a.DIAGNOSIS"));
				session.setRandevuDate(result.getDate("a.DATE").toString());
				session.setRandevuUptodate(result.getInt("a.DRAFT_UPDATED"));
				session.setRandevuNotes(result.getString("a.NOTES"));
				session.setUserId(result.getString("a.USER_ID"));
				String[] drug = new String[1];
				String[] side_effect = new String[1];
				Treatments treatment = new Treatments();
				drug[0] = result.getString("b.drug");
				side_effect[0] = result.getString("b.comment");
				treatment.setDrug(drug);
				treatment.setSideEffects(side_effect);
				session.setRandevuTreatment(treatment);
				session.setUserNames(result.getString("a.USERNAME"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Got an exception at session! ");
		    System.err.println(e.getMessage());
		}
		return session;
	}

	public String setDBSessionInfo(Randevu session, int patientId, String userId) {
		
		connect();
		try {
			Treatments treatments = new Treatments();
			treatments = session.getRandevuTreatment();
			String[] drug = treatments.getDrug(); 
			int treatment_id = 0;
			Statement stm = conn.createStatement();
			ResultSet result = stm.executeQuery("SELECT treatment_id from treatment where drug="+ drug[0]);
			while (result.next()){
				treatment_id = result.getInt("TREATMENT_ID");
			}								
			PreparedStatement preparedStmt  = (PreparedStatement) conn.prepareStatement("insert into EPL603.session values (default,?,?,?,?,?,?,?,?)");
			preparedStmt.setString(1, session.getRandevuCondition()); 
			preparedStmt.setInt(2, patientId);
			preparedStmt.setString(3, session.getRandevuDate());
			preparedStmt.setInt(4, session.getRandevuUptodate());
			preparedStmt.setString(5, session.getRandevuNotes());
			preparedStmt.setString(6, userId);
			preparedStmt.setInt(7, treatment_id);
			preparedStmt.setString(8, session.getUserNames());
			preparedStmt.executeUpdate();
						
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Got an exception at insert into session! ");
		    System.err.println(e.getMessage());
		    
		}
		return "success";
	}
	
}
