package systemServices;

import customDataTypes.User;

public interface MedicalRecordsService {
	public User setUser(String user, String pass);
	public User getUser(String userId);
	public String setRequest(String request);
	public void addTransaction(String transaction);
}
