package systemServices;

import customDataTypes.Asthenis;
import customDataTypes.Randevu;
import customDataTypes.Treatments;

public interface ClinicalStaffService {
	
	public Asthenis getDBPatientRecord(int patientId);
	public Treatments getDBTreatmentsInfo(String request);
	public Randevu getDBSessionInfo(int patientId, String sessionDate);
	public String setDBSessionInfo(Randevu session, int patientId, String userId);
	
}
