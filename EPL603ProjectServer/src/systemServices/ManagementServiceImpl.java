package systemServices;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import customDataTypes.ConditionsAndTreatments;
import customDataTypes.ReportData;
import customDataTypes.Treatments;

public class ManagementServiceImpl implements ManagementService{
	
	Connection conn; 
	public void connect(){
			try{
				Class.forName("com.mysql.jdbc.Driver");
				conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/EPL603?user=sqluser&password=sqluserpw");
			}catch(Exception e){
				e.printStackTrace();
			}
		}

	public ConditionsAndTreatments getDBPatientsConditionsAndTreatments(String request) {
		
		ConditionsAndTreatments conditionsAndTreatments = new ConditionsAndTreatments();
		connect();
		ArrayList<String> conditions = new ArrayList<>();
		Treatments treatments = new Treatments();
		ArrayList<String> drugs = new ArrayList<>();
		
		try {
			Statement stm = conn.createStatement();
			ResultSet result = stm.executeQuery("select a.diagnosis, b.drug ,b.comment from EPL603.session a, EPL603.treatment b where a.treatment_id=b.treatment_id");
			while (result.next()){
				conditions.add(result.getString("a.diagnosis"));
				drugs.add(result.getString("b.drug"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Got an exception at conditions and treatments! ");
		    System.err.println(e.getMessage());
		}
		
		treatments.setDrug(drugs.toArray(new String[drugs.size()]));
		conditionsAndTreatments.setConditions(conditions.toArray(new String[conditions.size()]));
		conditionsAndTreatments.setTreatments(treatments);
		
		return conditionsAndTreatments;
	}

	public ReportData getDBNumberOfPatientsEachDay(String request) {
		
		ReportData numberOfPatientsEachDay = new ReportData();
		connect();
		ArrayList<String> name = new ArrayList<>();
		ArrayList<Integer> numbers = new ArrayList<>();
		ArrayList<String> date = new ArrayList<>();
		
		try {
			Statement stm = conn.createStatement();
			ResultSet result = stm.executeQuery("select count(*), a.date, b.clinic_id from session a, users b where a.user_id=b.user_id group by a.date,b.clinic_id");
			while (result.next()){
				
				numbers.add(result.getInt("count(*)"));
				date.add(result.getDate("date").toString());
				name.add(result.getString("clinic_id"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Got an exception at patients per day and per clinic! ");
		    System.err.println(e.getMessage());
		}
		
		numberOfPatientsEachDay.setName(name.toArray(new String[name.size()]));
		numberOfPatientsEachDay.setNumber(convertIntegers(numbers));
		numberOfPatientsEachDay.setDate(date.toArray(new String[date.size()]));
		
		return numberOfPatientsEachDay;
	}

	public ReportData getDBNumberOfPatientsEachMonth(String request) {
		
		ReportData numberOfPatientsEachMonth = new ReportData();
		connect();
		ArrayList<String> name = new ArrayList<>();
		ArrayList<Integer> numbers = new ArrayList<>();
		ArrayList<String> date = new ArrayList<>();
		
		try {
			Statement stm = conn.createStatement();
			ResultSet result = stm.executeQuery("select count(*), month(a.date), b.clinic_id from session a, users b where a.user_id=b.user_id group by month(a.date),b.clinic_id");
			while (result.next()){
				
				numbers.add(result.getInt("count(*)"));
				date.add(result.getString("month(a.date).toString()").toString());
				name.add(result.getString("clinic_id"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Got an exception at patients per month and per clinic! ");
		    System.err.println(e.getMessage());
		}
		
		numberOfPatientsEachMonth.setName(name.toArray(new String[name.size()]));
		numberOfPatientsEachMonth.setNumber(convertIntegers(numbers));
		numberOfPatientsEachMonth.setDate(date.toArray(new String[date.size()]));

		return numberOfPatientsEachMonth;
	}

	public ReportData getDBNumberOfPatientsEachCondition(String request) {
		
		ReportData numberOfPatientsEachCondition = new ReportData();
		connect();
		ArrayList<String> name = new ArrayList<>();
		ArrayList<Integer> numbers = new ArrayList<>();
		
		try {
			Statement stm = conn.createStatement();
			ResultSet result = stm.executeQuery("select count(*), diagnosis from session group by diagnosis");
			while (result.next()){
				
				numbers.add(result.getInt("count(*)"));
				name.add(result.getString("diagnosis"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Got an exception at patients per conditions! ");
		    System.err.println(e.getMessage());
		}
		
		numberOfPatientsEachCondition.setName(name.toArray(new String[name.size()]));
		numberOfPatientsEachCondition.setNumber(convertIntegers(numbers));
		
		return numberOfPatientsEachCondition;
	}

	public ReportData getDBNumberOfEachDrugPrescribed(String request) {
		
		ReportData numberOfEachDrugPrescribed = new ReportData();
		connect();
		ArrayList<String> name = new ArrayList<>();
		ArrayList<Integer> numbers = new ArrayList<>();
	
		try {
			Statement stm = conn.createStatement();
			ResultSet result = stm.executeQuery("select count(*), a.treatment_id, b.drug from session a, treatment b where a.treatment_id=b.treatment_id group by treatment_id");
			while (result.next()){
				
				numbers.add(result.getInt("count(*)"));
				name.add(result.getString("b.drug"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Got an exception at patients per day and per clinic! ");
		    System.err.println(e.getMessage());
		}
		
		numberOfEachDrugPrescribed.setName(name.toArray(new String[name.size()]));
		numberOfEachDrugPrescribed.setNumber(convertIntegers(numbers));

		return numberOfEachDrugPrescribed;
	}
	
	private static int[] convertIntegers(ArrayList<Integer> integers)
	{
	    int[] ret = new int[integers.size()];
	    Iterator<Integer> iterator = integers.iterator();
	    for (int i = 0; i < ret.length; i++)
	    {
	        ret[i] = iterator.next().intValue();
	    }
	    return ret;
	}

}
