package systemServices;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;

import customDataTypes.Asthenis;
import customDataTypes.Kratisi;
import customDataTypes.User;

public class ReceptionistsServiceImpl implements ReceptionistsService{
	
	Connection conn; 
	public void connect(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/EPL603?user=sqluser&password=sqluserpw");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public Kratisi[] getDBAppointments(String date) {
		
		ArrayList<Kratisi> appointments = new ArrayList<>();
		connect();
		Asthenis patient = new Asthenis();
		User user = new User();
		
		try {
			Statement stm = conn.createStatement();
			ResultSet result = stm.executeQuery("SELECT * FROM appoitment where date='"+ date +"'");
			while (result.next()){
				Kratisi appointment = new Kratisi();
				appointment.setKratisiDate(result.getDate("DATE").toString());// problem me to array
				appointment.setKratisiStatus(result.getString("appoitment_status"));
				Statement stm1 = conn.createStatement();
				ResultSet result1 = stm1.executeQuery("SELECT * FROM patient where id_patient='1'");
				while (result1.next()){
					patient.setAsthenisId(result1.getInt("ID_PATIENT"));
					patient.setAsthenisName(result1.getString("NAME"));
					patient.setAsthenisSurname(result1.getString("SURNAME"));
					patient.setAsthenisAddress(result1.getString("ADREESS"));
					patient.setAsthenisPhone(result1.getString("PHONE"));
					patient.setAsthenisRiskStatus(result1.getInt("RISK_STATUS"));
					patient.setAsthenisAllergies(result1.getString("ALLERGIES"));
					patient.setAsthenisStatus(result1.getInt("STATUS"));
				}
				appointment.setKratisiPatient(patient);
				Statement stm2 = conn.createStatement();
				ResultSet result2 = stm2.executeQuery("SELECT * FROM users where user_id='"+ result.getString("user_id") +"'");
				while (result2.next()){
					user.setUserName(result2.getString("NAME"));
					user.setUserSurname(result2.getString("SURNAME"));
					user.setUserClinic(result2.getString("CLINIC_ID"));
					user.setUserType(result2.getInt("TYPE"));
					user.setUserId(result2.getString("USER_ID"));
				}
				appointment.setKratisiUser(user);
				appointments.add(appointment);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Got an exception at appoitments! ");
		    System.err.println(e.getMessage());
		}
		
		return appointments.toArray(new Kratisi[appointments.size()]);
	}

	public Asthenis getDBPatientRecord(int patientId) {
		
		Asthenis patientRecord = new Asthenis();
		connect();
		
		try {
			Statement stm = conn.createStatement();
			ResultSet result = stm.executeQuery("SELECT * FROM EPL603.patient where id_patient="+ patientId);
			while (result.next()){
				
				patientRecord.setAsthenisId(result.getInt("ID_PATIENT"));
				patientRecord.setAsthenisName(result.getString("NAME"));
				patientRecord.setAsthenisSurname(result.getString("SURNAME"));
				patientRecord.setAsthenisAddress(result.getString("ADREESS"));
				patientRecord.setAsthenisPhone(result.getString("PHONE"));
				patientRecord.setAsthenisRiskStatus(result.getInt("RISK_STATUS"));
				patientRecord.setAsthenisAllergies(result.getString("ALLERGIES"));
				patientRecord.setAsthenisStatus(result.getInt("STATUS"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Got an exception at patient! ");
		    System.err.println(e.getMessage());
		}
		
		return patientRecord;
	}

	public String setDBPatientAppointment(Kratisi appointment) {
				
		connect();
				
		try {
			PreparedStatement preparedStmt  = (PreparedStatement) conn.prepareStatement("insert into EPL603.appoitment values (default,?,?,?,?)");
			preparedStmt.setString(1, appointment.getKratisiDate());
			Asthenis patient = appointment.getKratisiPatient();
			preparedStmt.setInt(2, patient.getAsthenisId());
			User user = appointment.getKratisiUser();
			preparedStmt.setString(3, user.getUserId());
			preparedStmt.setString(4, appointment.getKratisiStatus());
						
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Got an exception at insert into appoitmnet! ");
		    System.err.println(e.getMessage());
		}
		
		return "success";
	}

	public String setDBPatient(Asthenis patient) {
		
		connect();
		
		try {
			PreparedStatement preparedStmt  = (PreparedStatement) conn.prepareStatement("insert into EPL603.patients values (default,?,?,?,?,?,?,?)");
			preparedStmt.setString(1, patient.getAsthenisName());
			preparedStmt.setString(2, patient.getAsthenisSurname());
			preparedStmt.setString(3, patient.getAsthenisAddress());
			preparedStmt.setString(4, patient.getAsthenisPhone());
			preparedStmt.setInt(5, patient.getAsthenisRiskStatus());
			preparedStmt.setString(6, patient.getAsthenisAllergies());
			preparedStmt.setInt(7, patient.getAsthenisStatus());
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Got an exception at insert into patient! ");
		    System.err.println(e.getMessage());
		}
		
		return "success";
	}

}
