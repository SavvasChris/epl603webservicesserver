package customDataTypes;

public class Asthenis {
	private int asthenisId;
	private String asthenisName;
	private String asthenisSurname;
	private String asthenisAddress;
	private String asthenisPhone;
	private int asthenisRiskStatus;
	private String asthenisAllergies;
	private int asthenisStatus;
	public int getAsthenisId() {
		return asthenisId;
	}
	public void setAsthenisId(int asthenisId) {
		this.asthenisId = asthenisId;
	}
	public String getAsthenisName() {
		return asthenisName;
	}
	public void setAsthenisName(String asthenisName) {
		this.asthenisName = asthenisName;
	}
	public String getAsthenisSurname() {
		return asthenisSurname;
	}
	public void setAsthenisSurname(String asthenisSurname) {
		this.asthenisSurname = asthenisSurname;
	}
	public String getAsthenisAddress() {
		return asthenisAddress;
	}
	public void setAsthenisAddress(String asthenisAddress) {
		this.asthenisAddress = asthenisAddress;
	}
	public String getAsthenisPhone() {
		return asthenisPhone;
	}
	public void setAsthenisPhone(String asthenisPhone) {
		this.asthenisPhone = asthenisPhone;
	}
	public int getAsthenisRiskStatus() {
		return asthenisRiskStatus;
	}
	public void setAsthenisRiskStatus(int asthenisRiskStatus) {
		this.asthenisRiskStatus = asthenisRiskStatus;
	}
	public String getAsthenisAllergies() {
		return asthenisAllergies;
	}
	public void setAsthenisAllergies(String asthenisAllergies) {
		this.asthenisAllergies = asthenisAllergies;
	}
	public int getAsthenisStatus() {
		return asthenisStatus;
	}
	public void setAsthenisStatus(int asthenisStatus) {
		this.asthenisStatus = asthenisStatus;
	}
	
	
}
