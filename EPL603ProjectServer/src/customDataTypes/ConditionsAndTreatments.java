package customDataTypes;

public class ConditionsAndTreatments {
	private String[] conditions;
	private Treatments treatments;
	
	public String[] getConditions() {
		return conditions;
	}
	public void setConditions(String[] conditions) {
		this.conditions = conditions;
	}
	public Treatments getTreatments() {
		return treatments;
	}
	public void setTreatments(Treatments treatments) {
		this.treatments = treatments;
	}
	
	
}
