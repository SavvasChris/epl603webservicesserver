package customDataTypes;

public class Appointment {
	private String appointmentDate;
	private String appointmentStatus;
	private Patient appointmentPatient;
	private User appointmentUser;
	
	public String getAppointmentDate() {
		return appointmentDate;
	}
	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}
	public String getAppointmentStatus() {
		return appointmentStatus;
	}
	public void setAppointmentStatus(String appointmentStatus) {
		this.appointmentStatus = appointmentStatus;
	}
	public Patient getAppointmentPatient() {
		return appointmentPatient;
	}
	public void setAppointmentPatient(Patient appointmentPatient) {
		this.appointmentPatient = appointmentPatient;
	}
	public User getAppointmentUser() {
		return appointmentUser;
	}
	public void setAppointmentUser(User appointmentUser) {
		this.appointmentUser = appointmentUser;
	}
	
	
}
