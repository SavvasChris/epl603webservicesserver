package customDataTypes;

public class Kratisi {
	private String kratisiDate;
	private String kratisiStatus;
	private Asthenis kratisiPatient;
	private User kratisiUser;
	
	public String getKratisiDate() {
		return kratisiDate;
	}
	public void setKratisiDate(String kratisiDate) {
		this.kratisiDate = kratisiDate;
	}
	public String getKratisiStatus() {
		return kratisiStatus;
	}
	public void setKratisiStatus(String kratisiStatus) {
		this.kratisiStatus = kratisiStatus;
	}
	public Asthenis getKratisiPatient() {
		return kratisiPatient;
	}
	public void setKratisiPatient(Asthenis kratisiPatient) {
		this.kratisiPatient = kratisiPatient;
	}
	public User getKratisiUser() {
		return kratisiUser;
	}
	public void setKratisiUser(User kratisiUser) {
		this.kratisiUser = kratisiUser;
	}
}
