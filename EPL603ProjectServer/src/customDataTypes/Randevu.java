package customDataTypes;

public class Randevu {
	private int randevuId;
	private String randevuCondition;
	private Treatments randevuTreatment;
	private String randevuDate;
	private int randevuUptodate;
	private String randevuNotes;
	private String userNames;
	private String userId;
	
	public int getRandevuId() {
		return randevuId;
	}
	public void setRandevuId(int randevuId) {
		this.randevuId = randevuId;
	}
	public String getRandevuCondition() {
		return randevuCondition;
	}
	public void setRandevuCondition(String randevuCondition) {
		this.randevuCondition = randevuCondition;
	}
	public Treatments getRandevuTreatment() {
		return randevuTreatment;
	}
	public void setRandevuTreatment(Treatments randevuTreatment) {
		this.randevuTreatment = randevuTreatment;
	}
	public String getRandevuDate() {
		return randevuDate;
	}
	public void setRandevuDate(String randevuDate) {
		this.randevuDate = randevuDate;
	}
	public int getRandevuUptodate() {
		return randevuUptodate;
	}
	public void setRandevuUptodate(int randevuUptodate) {
		this.randevuUptodate = randevuUptodate;
	}
	public String getRandevuNotes() {
		return randevuNotes;
	}
	public void setRandevuNotes(String randevuNotes) {
		this.randevuNotes = randevuNotes;
	}
	public String getUserNames() {
		return userNames;
	}
	public void setUserNames(String userNames) {
		this.userNames = userNames;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
