package customDataTypes;

public class Patient {
	private int patientId;
	private String patientName;
	private String patientSurname;
	private String patientAddress;
	private String patientPhone;
	private int patientRiskStatus;
	private String patientAllergies;
	private int patientStatus;
	
	public int getPatientId() {
		return patientId;
	}
	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getPatientSurname() {
		return patientSurname;
	}
	public void setPatientSurname(String patientSurname) {
		this.patientSurname = patientSurname;
	}
	public String getPatientAddress() {
		return patientAddress;
	}
	public void setPatientAddress(String patientAddress) {
		this.patientAddress = patientAddress;
	}
	public String getPatientPhone() {
		return patientPhone;
	}
	public void setPatientPhone(String patientPhone) {
		this.patientPhone = patientPhone;
	}
	public int getPatientRiskStatus() {
		return patientRiskStatus;
	}
	public void setPatientRiskStatus(int patientRiskStatus) {
		this.patientRiskStatus = patientRiskStatus;
	}
	public String getPatientAllergies() {
		return patientAllergies;
	}
	public void setPatientAllergies(String patientAllergies) {
		this.patientAllergies = patientAllergies;
	}
	public int isPatientStatus() {
		return patientStatus;
	}
	public void setPatientStatus(int patientStatus) {
		this.patientStatus = patientStatus;
	}
}
