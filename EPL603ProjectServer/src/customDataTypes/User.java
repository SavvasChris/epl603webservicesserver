package customDataTypes;

public class User {
	
	private String userId;
	private String userName;
	private String userSurname;
	private int userType;
	private String userClinic;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserSurname() {
		return userSurname;
	}
	public void setUserSurname(String userSurname) {
		this.userSurname = userSurname;
	}
	public int getUserType() {
		return userType;
	}
	public void setUserType(int userType) {
		this.userType = userType;
	}
	public String getUserClinic() {
		return userClinic;
	}
	public void setUserClinic(String userClinic) {
		this.userClinic = userClinic;
	}
	
}
